<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'roles_id' => '1',
            'name' => 'Fidelity',
            'email' => 'admin@email.com',
            'password' => Hash::make('12345678'),
            'status' => '1',
        ]);
    }
}
