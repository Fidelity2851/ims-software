<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SuppliersMails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers-mails', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('mail_id');
            $table->string('seen')->default('0');
            $table->timestamps();
        });
        /*Schema::table('suppliers-mails', function (Blueprint $table) {
            $table->foreignId('supplier_id')->references('id')->on('supplier')->onDelete('cascade');
            $table->foreignId('mail_id')->references('id')->on('mails')->onDelete('cascade');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers-mails');
    }
}
