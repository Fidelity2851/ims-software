<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('roles_id')->nullable();
            $table->string('name')->unique()->default('fidelity');
            $table->string('email')->unique()->default('admin@admin.com');
            $table->text('phone')->nullable();
            $table->string('password')->default('12345678');
            $table->text('image')->nullable();
            $table->text('bio')->nullable();
            $table->date('dob')->nullable();
            $table->string('status')->default('1');
            $table->rememberToken();
            $table->timestamps();
        });

        /*Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
