<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controllers\dashboard::class, 'home'] )->name('home');

Route::any('/dashboard', [Controllers\dashboard::class, 'index'] )->name('dashboard');

Route::get('/pos', [Controllers\pos::class, 'index'] )->name('pos');

Route::post('/clock', [Controllers\login::class, 'login'] )->name('clock');

Route::get('/category', [Controllers\category::class, 'index'] )->name('category');

Route::get('/subcategory', [Controllers\subcategory::class, 'index'] )->name('subcategory');

Route::get('/products', [Controllers\product::class, 'index'] )->name('products');

Route::get('/purchases', [Controllers\purchase::class, 'index'] )->name('purchases');

Route::get('/customers', [Controllers\customer::class, 'index'] )->name('customers');

Route::get('/suppliers', [Controllers\supplier::class, 'index'] )->name('suppliers');

Route::get('/workers', [Controllers\worker::class, 'index'] )->name('workers');

Route::get('/roles', [Controllers\role::class, 'index'] )->name('roles');

Route::get('/mails', [Controllers\mail::class, 'index'] )->name('mails');

Route::get('/messages', [Controllers\message::class, 'index'] )->name('messages');

Route::get('/accounts', [Controllers\account::class, 'index'] )->name('accounts');

Route::get('/settings/general', [Controllers\setting::class, 'index'] )->name('gen_setting');



/*
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');*/
