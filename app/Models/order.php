<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    use HasFactory;

    public function product(){

        return $this->belongsToMany('App\Model\products', 'receipts', 'order_id', 'product_id')->withPivot('price')->withTimestamps();
    }

    public function customer(){

        return $this->belongsTo('App\Models\customer','customer_id');
    }

}
