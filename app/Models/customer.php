<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function mail(){

        return $this->belongsToMany('App\Models\mail', 'customers-mails', 'customer_id', 'mail_id')->withTimestamps();
    }

    public function order(){

        return $this->hasMany('App\Models\order', 'customer_id');
    }

    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
                ->where('name', 'like', '%'.$search.'%')
                ->orWhere('email', 'like', '%'.$search.'%');
    }

}
