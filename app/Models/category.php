<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class category extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function subcategory(){

        return $this->belongsToMany('App\Models\subcategory', 'cate-sub_cate', 'category_id', 'subcategory_id');
    }

    public function product(){

        return $this->hasMany('App\Models\product', 'category_id');
    }

    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
            ->where('name', 'like', '%'.$search.'%')
            ->orWhere('description', 'like', '%'.$search.'%');
    }

}
