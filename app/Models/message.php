<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class message extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user(){

        return $this->belongsToMany('App\Models\User', 'users-messages', 'message_id', 'user_id');
    }

    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
                ->where('body', 'like', '%'.$search.'%');
    }
}
