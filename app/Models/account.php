<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class account extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user(){

        return $this->belongsToMany('App\Models\User', 'users-accounts', 'account_id', 'user_id')->withPivot('salary')->withTimestamps();
    }

    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
            ->where('payment_number', 'like', '%'.$search.'%');
    }

}
