<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subcategory extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function category(){

        return $this->belongsToMany('App\Models\category', 'cate-sub_cate', 'subcategory_id', 'category_id');
    }

    public function product(){

        return $this->hasMany('App\Models\product', 'subcategory_id');
    }

    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
                ->where('name', 'like', '%'.$search.'%')
                ->orWhere('description', 'like', '%'.$search.'%');
    }

}
