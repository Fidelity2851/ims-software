<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class worker extends Model
{
    use HasFactory;



    public function role(){

        return $this->hasOne('App\Models\role', 'role_id');
    }

    public function message(){

        return $this->belongsToMany('App\Model\message', 'workers_messages', 'worker_id', 'message_id')->withTimestamps();
    }

    public function account(){

        return $this->belongsToMany('App\Models\account', 'workers-accounts', 'worker_id', 'account_id')->withTimestamps();
    }

    public function clocking(){

        return $this->hasMany('App\Models\clocking', 'worker_id');
    }



}
