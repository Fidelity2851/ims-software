<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class supplier extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function product(){

        return $this->hasMany('App\Models\product', 'supplier_id');
    }

    public function mail(){

        return $this->belongsToMany('App\Models\mail', 'suppliers-mails', 'supplier_id', 'mail_id')->withTimestamps();
    }

    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
                ->where('name', 'like', '%'.$search.'%')
                ->orWhere('address', 'like', '%'.$search.'%');
    }

}
