<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mail extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function customer(){

        return $this->belongsToMany('App\Models\customer', 'customers-mails', 'mail_id', 'customer_id')->withTimestamps();
    }

    public function supplier(){

        return $this->belongsToMany('App\Models\supplier', 'suppliers-mails', 'mail_id', 'supplier_id')->withTimestamps();
    }

    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
                ->where('body', 'like', '%'.$search.'%');
    }

}
