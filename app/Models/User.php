<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password',
    ];*/
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


    public function roles(){

        return $this->belongsTo('App\Models\roles', 'roles_id');
    }

    public function message(){

        return $this->belongsToMany('App\Models\message', 'users_messages', 'user_id', 'message_id')->withTimestamps();
    }

    public function account(){

        return $this->belongsToMany('App\Models\account', 'users-accounts', 'user_id', 'account_id')->withPivot('salary')->withTimestamps();
    }

    public function clocking(){

        return $this->hasMany('App\Models\clocking', 'user_id');
    }

    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
                ->where('name', 'like', '%'.$search.'%')
                ->orWhere('bio', 'like', '%'.$search.'%');
    }

}
