<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function category(){

        return $this->belongsTo('App\Models\category', 'category_id');
    }

    public function subcategory(){

        return $this->belongsTo('App\Models\subcategory', 'subcategory_id');
    }

    public function supplier(){

        return $this->belongsTo('App\Models\supplier', 'supplier_id');
    }

    public function order(){

        return $this->belongsToMany('App\Model\order', 'receipts', 'product_id', 'order_id')->withPivot('price')->withTimestamps();
    }


    public static function Search($search){
        return empty($search)
            ? static::query()
            : static::query()
                ->where('name', 'like', '%'.$search.'%')
                ->orWhere('description', 'like', '%'.$search.'%');
    }

}
