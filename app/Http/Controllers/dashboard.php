<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class dashboard extends Controller
{


    public function __construct(){

        $this->middleware('auth')->except('home');
    }

    public function home(){

        if (Auth::check()) {
            return view('dashboard');
        }
        else {
            return view('home');
        }


    }

    public function index(){

        return view('dashboard');
    }
}
