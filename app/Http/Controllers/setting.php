<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class setting extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    public function index(){

        return view('general-setting');
    }
}
