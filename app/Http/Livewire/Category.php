<?php

namespace App\Http\Livewire;


use App\Models\User;
use App\Models\subcategory;
use Illuminate\Http\Request;
use Livewire\WithPagination;
use Illuminate\Support\Collection;
use Livewire\Component;

class Category extends Component
{
    use WithPagination;

    public $createcategory = false;
    public $deletecategory = false;
    public $delete_id;

    public $checkall ;
    public $check = [];
    public $category = [];

    public $search ;
    public $name = '';
    public $description = '';
    public $status = 1 ;

    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15;


    protected function getListeners()
    {
        return ['update' => 'category_update'];
    }

    public function mount(){

    }


    public function show_category_modal(){
        $this->createcategory = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deletecategory = true;
    }

    public function close_category_modal(){
        $this->reset(['name', 'description', 'status']);
        $this->resetErrorBag();
        $this->createcategory = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deletecategory = false;
    }

    public  function createcategory(){
        $this->validate([
            'name' => 'required|unique:categories|max:255',
            'description' => 'required|max:255',
            'status' => '',
        ]);

        \App\Models\category::create([
           'name' => $this->name,
           'description' => $this->description,
           'status' => $this->status != null ? $this->status : '0',
        ]);
        $this->reset(['name', 'description', 'status']);
        session()->flash('message', 'Category successfully Created.');


    }

    public function delete_category($id){
        $this->deletecategory = false;
        \App\Models\category::FindorFail($id)->delete();
        $this->reset('delete_id');
        session()->flash('del_message', 'Category Successfully Deleted.');
    }


    public function multi_delete(){
        dd($this->check);
    }

    public function category_update(){
        $this->render();
    }

    public function updatingSearch(){
        $this->resetPage();
    }


    public function render()
    {
        $this->category = \App\Models\category::where('status', '1')->get();
        $category =   \App\Models\category::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);

        return view('livewire.category', [
            'categories' => $category,
        ]);
    }
}
