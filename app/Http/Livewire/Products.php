<?php

namespace App\Http\Livewire;


use App\Models\product;
use App\Models\category;
use App\Models\subcategory;
use App\Models\supplier;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Livewire\Component;

class Products extends Component
{
    use WithPagination;
    use WithFileUploads;


    public $createproducts = false;
    public $deleteproducts = false;
    public $delete_id;

    public $check = [];
    public $subcategories = [];


    public $name = '';
    public $quantity ;
    public $category ;
    public $subcategory ;
    public $supplier ;
    public $image = '';
    public $price ;
    public $description = '';
    public $status = 1 ;

    public $search ;
    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15 ;


    protected function getListeners()
    {
        return ['update' => 'product_update'];
    }

    public function mount(){
        
    }


    public function show_products_modal(){
        $this->createproducts = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deleteproducts = true;
    }

    public function close_products_modal(){
        $this->reset(['name', 'quantity', 'category', 'subcategory', 'subcategories', 'supplier', 'image', 'price', 'description', 'status']);
        $this->resetErrorBag();
        $this->createproducts = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deleteproducts = false;
    }

    public function remove_image(){
        $this->reset('image');
    }

    public function create_products(){
        $this->validate([
            'name' => 'required|unique:products|max:255',
            'quantity' => 'required|integer',
            'category' => 'required',
            'subcategory' => 'required',
            'supplier' => 'required',
            'image' => 'required|file|image|max:2048',
            'price' => 'required|integer',
            'description' => 'required|max:255',
            'status' => '',
        ]);

        /*get original image name*/
        $getname= $this->image->getClientOriginalName();

        /*concatinate with a random time*/
        $imgname = time() . $getname;
        $this->image->storeAs('public/products', $imgname);
        /* Resizing Image with intervention image */
        Image::make('storage/products/'.$imgname)->resize(100, 100)->save();

        product::create([
            'name' => $this->name,
            'quantity' => $this->quantity,
            'category_id' => $this->category,
            'subcategory_id' => $this->subcategory,
            'supplier_id' => $this->supplier,
            'image' => $imgname,
            'price' => $this->price,
            'description' => $this->description,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        $this->reset(['name', 'quantity', 'category', 'subcategory', 'subcategories', 'supplier', 'image', 'price', 'description', 'status']);
        session()->flash('message', 'Products Successfully Created.');
    }

    public function delete_products(product $product){
        $getimg = Storage::exists('public/products/' . $product->image);
        $product->delete();
        if ($getimg){
            Storage::delete('public/products/' . $product->image );
        }

        $this->deleteproducts = false;
        $this->reset('delete_id');
        session()->flash('del_message', 'Product successfully Deleted.');
    }

    public function multi_delete(){

        dd($this->check);

    }



    public function updatedCategory(){
        if($this->category != null){
            $subcategory = category::find($this->category)->subcategory()->get();
            $this->subcategories = $subcategory;
        }
        else {
            $this->subcategories = [];
        }
    }

    public function updatingSearch(){
        $this->resetPage();
    }

    public function product_update(){
        $this->render();
    }

    public function render()
    {
        $category = category::where('status', '1')->get();

        $supplier = supplier::where('status', '1')->get();

        $product =   product::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);


        return view('livewire.products', [
            'categories' => $category,
            'suppliers' => $supplier,
            'products' => $product,
        ]);
    }
}
