<?php

namespace App\Http\Livewire;

use App\Models\roles;
use App\Models\receipt;
use App\Models\supplier;
use App\Models\User;
use Livewire\Component;

class Dashboard extends Component
{
    public $sales;
    public $profits;
    public $suppliers;
    public $salary;

    public function mount(){

        $this->sales = receipt::where('status', '1')->count();
        $this->profits = receipt::where('status', '1')->count();
        $this->suppliers = supplier::where('status', '1')->count();
        $payment = roles::with('user')->where('status', '1')->get();
        $this->salary = $payment->sum('salary');


    }


    public function render()
    {
        return view('livewire.dashboard');
    }
}
