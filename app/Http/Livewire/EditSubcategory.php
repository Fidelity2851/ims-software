<?php

namespace App\Http\Livewire;

use App\Models\category;
use App\Models\subcategory;
use Livewire\Component;

class EditSubcategory extends Component
{

    public $editsubcategory = false;

    public $subcategory_id = null;
    public $name ;
    public $category = [];
    public $description ;
    public $status ;


    protected function getListeners()
    {
        return ['show_modal' => 'show_editsubcategory_modal'];
    }

    public function mount(){

    }


    public function show_editsubcategory_modal(subcategory $subcategory){
    

        $this->subcategory_id = $subcategory->id;
        $this->editsubcategory = true;

        $this->name = $subcategory->name;
        $this->description = $subcategory->description;
        if ($subcategory->status == 1){
            $this->status = true;
        }
        else{
            $this->status = false;
        }

    }

    public function close_editsubcategory_modal(){
        $this->reset(['subcategory_id','name', 'category', 'description', 'status']);
        $this->resetErrorBag();
        $this->editsubcategory = false;
    }


    public function updatecategory(subcategory $subcategory){
        $this->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'status' => '',
        ]);
        

        $data = $subcategory->update([
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status != null ? $this->status : '0',
        ]);
        
        $subcategory->category()->sync($this->category);
        $this->resetErrorBag();
        session()->flash('message', 'Update successfully.');
        $this->emitTo('subcategory', 'update');
    }


    public function render()
    {
        $category = category::where('status', '1')->get();

        return view('livewire.edit-subcategory', [
            'categories' => $category,
        ]);
    }

}
