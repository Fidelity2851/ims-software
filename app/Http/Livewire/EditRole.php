<?php

namespace App\Http\Livewire;

use \App\Models\roles;
use Livewire\Component;

class EditRole extends Component
{
    public $editrole = false;

    public $role_id = null;
    public $name ;
    public $salary ;
    public $description ;
    public $status ;


    protected function getListeners()
    {
        return ['show_modal' => 'show_editrole_modal'];
    }

    public function mount(){

    }


    public function show_editrole_modal(roles $role){
        $this->role_id = $role->id;
        $this->editrole = true;

        $this->name = $role->name;
        $this->salary = $role->salary;
        $this->description = $role->description;
        if ($role->status == 1){
            $this->status = true;
        }
        else{
            $this->status = false;
        }

    }

    public function close_editrole_modal(){
        $this->reset(['role_id','name', 'salary','description', 'status']);
        $this->resetErrorBag();
        $this->editrole = false;
    }

    public function update_role(roles $role){
        $this->validate([
            'name' => 'required|max:255',
            'salary' => 'required|integer',
            'description' => 'required|max:255',
            'status' => '',
        ]);

        $role->update([
            'name' => $this->name,
            'salary' => $this->salary,
            'description' => $this->description,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        $this->resetErrorBag();
        session()->flash('message', 'Update successfully.');
        $this->emitTo('roles', 'update');
    }


    public function render()
    {
        return view('livewire.edit-role');
    }
}
