<?php

namespace App\Http\Livewire;

use App\Models\message;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Messages extends Component
{
    use WithPagination;

    public $createmessage = false;
    public $deletemessage = false;
    public $viewmessage = false;
    public $delete_id;

    public $checkall ;
    public $check = [];
    public $category = [];

    public $search ;
    public $worker = [];
    public $body = '';
    public $status = 1 ;

    public $messageworker ;
    public $message ;

    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15;



    public function mount(){

    }


    public function show_message_modal(){
        $this->createmessage = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deletemessage = true;
    }

    public function close_message_modal(){
        $this->reset(['worker', 'body', 'status']);
        $this->resetErrorBag();
        $this->createmessage = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deletemessage = false;
    }

    public function createmessage(){
        $this->validate([
            'worker' => 'required|max:255',
            'body' => 'required',
            'status' => '',
        ]);

        $msg = message::create([
            'body' => $this->body,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        if ($this->worker != []){
            foreach ($this->worker as $work){
                $msg->user()->attach($work);
            }
        }

        $this->reset(['worker', 'body', 'status']);
        session()->flash('message', 'Message Sent successfully.');

    }

    public function delete_message($id){
        $this->deletemessage = false;
        message::FindorFail($id)->delete();
        $this->reset('delete_id');
        session()->flash('del_message', 'Message successfully Deleted.');
    }

    public function view_message(message $message){
        $this->viewmessage = true;
        $this->messageworker = $message->user;
        $this->message = $message->body;
    }

    public function close_message(){
        $this->reset(['messageworker', 'message']);
        $this->viewmessage = false;
    }

    public function updatingSearch(){
        $this->resetPage();
    }


    public function render()
    {
        $worker = User::where('status', '1')->get();
        $message = message::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);

        return view('livewire.messages', [
            'workers' => $worker,
            'messages' => $message,
        ]);
    }
}
