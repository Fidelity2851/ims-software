<?php

namespace App\Http\Livewire;

use App\Models\supplier;
use Livewire\Component;

class EditSupplier extends Component
{

    public $editsupplier = false;

    public $supplier_id = null;
    public $name ;
    public $phone ;
    public $email ;
    public $address ;
    public $status ;


    protected function getListeners()
    {
        return ['show_modal' => 'show_editsupplier_modal'];
    }

    public function mount(){

    }


    public function show_editsupplier_modal(supplier $supplier){
        $this->supplier_id = $supplier->id;
        $this->editsupplier = true;
        $this->name = $supplier->name;
        $this->phone = $supplier->phone;
        $this->email = $supplier->email;
        $this->address = $supplier->address;
        if ($supplier->status == 1){
            $this->status = true;
        }
        else{
            $this->status = false;
        }

    }

    public function close_editsupplier_modal(){
        $this->reset(['supplier_id', 'name', 'phone', 'email', 'address', 'status']);
        $this->resetErrorBag();
        $this->editsupplier = false;
    }

    public function updatesupplier(supplier $supplier){
        $this->validate([
            'name' => 'required|max:255',
            'phone' => 'required|integer',
            'email' => 'required|email',
            'address' => 'required|max:255',
            'status' => '',
        ]);

        $supplier->update([
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->address,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        /*$this->reset(['name', 'description', 'status']);*/
        $this->resetErrorBag();
        session()->flash('message', 'Updated successfully.');
        $this->emitTo('suppliers', 'update');
    }


    public function render()
    {
        return view('livewire.edit-supplier');
    }
}
