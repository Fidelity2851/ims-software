<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Purchases extends Component
{
    public function render()
    {
        return view('livewire.purchases');
    }
}
