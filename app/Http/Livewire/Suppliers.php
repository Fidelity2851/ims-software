<?php

namespace App\Http\Livewire;

use App\Models\supplier;
use Livewire\WithPagination;
use Livewire\Component;

class Suppliers extends Component
{
    use WithPagination;

    public $createsupplier = false;
    public $deletesupplier = false;
    public $delete_id;

    public $search ;

    public $name ;
    public $phone ;
    public $email ;
    public $address ;
    public $status = 1 ;

    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15 ;


    protected function getListeners()
    {
        return ['update' => 'supplier_update'];
    }


    public function mount(){

    }


    public function show_supplier_modal(){
        $this->createsupplier = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deletesupplier = true;
    }

    public function close_supplier_modal(){
        $this->reset(['name', 'phone', 'email', 'address', 'status']);
        $this->resetErrorBag();
        $this->createsupplier = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deletesupplier = false;
    }

    public  function createsuppliers(){
        $this->validate([
            'name' => 'required|unique:products|max:255',
            'phone' => 'required|integer',
            'email' => 'required|email',
            'address' => 'required|max:255',
            'status' => '',
        ]);

         supplier::create([
             'name' => $this->name,
             'phone' => $this->phone,
             'email' => $this->email,
             'address' => $this->address,
             'status' => $this->status != null ? $this->status : '0',
         ]);

        $this->reset(['name', 'phone', 'email', 'address', 'status']);
        session()->flash('message', 'Supplier successfully Created.');


    }

    public function delete_supplier($id){
        $this->deletesupplier = false;
        supplier::FindorFail($id)->delete();
        $this->reset('delete_id');
        session()->flash('del_message', 'Supplier successfully Delete.');
    }


    public function multi_delete(){

        dd($this->check);

    }

    public function supplier_update(){
        $this->render();
    }

    public function updatingSearch(){
        $this->resetPage();
    }


    public function render()
    {
        $supplier =   supplier::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);

        return view('livewire.suppliers', [
            'suppliers' => $supplier,
        ]);
    }
}
