<?php

namespace App\Http\Livewire;

use App\Models\category;
use App\Models\product;
use App\Models\supplier;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;
use Intervention\Image\Facades\Image;
use Livewire\Component;

class EditProduct extends Component
{
    use WithFileUploads;

    public $editproduct = false;

    public $subcategories = [];
    public $view_image ;

    public $product_id = null;
    public $name = '';
    public $quantity ;
    public $price ;

    public $oldcategory ;
    public $oldsubcategory ;
    public $oldsupplier ;

    public $category ;
    public $subcategory ;
    public $supplier ;

    public $image ;
    public $description = '';
    public $status = 1 ;


    protected function getListeners()
    {
        return ['show_modal' => 'show_editproducts_modal'];
    }

    public function mount(){

    }


    public function show_editproducts_modal(product $product){
        $this->editproduct = true;
        $this->product_id = $product->id;

        $this->name = $product->name;
        $this->quantity = $product->quantity;
        $this->category = $product->category->id;
        $this->subcategory = $product->subcategory->id;
        $this->supplier = $product->supplier->id;
        $this->view_image = $product->image;
        $this->price = $product->price;
        $this->description = $product->description;
        if ($product->status == 1){
            $this->status = true;
        }
        else{
            $this->status = false;
        }
        $subcategory = category::find($this->category)->subcategory()->get();
        $this->subcategories = $subcategory;

    }

    public function close_editproducts_modal(){
        $this->reset(['name', 'quantity', 'category', 'subcategory', 'supplier','image', 'price', 'description', 'status', 'view_image']);
        $this->resetErrorBag();
        $this->editproduct = false;
    }


    public function remove_image(){
        $this->reset('image');
    }

    public function update_products(product $product){
        $this->validate([
            'name' => 'required|max:255',
            'quantity' => 'required|integer',
            'category' => 'required',
            'subcategory' => 'required',
            'supplier' => 'required',
            'image' => 'sometimes|max:2048',
            'price' => 'required|integer',
            'description' => 'required|max:255',
            'status' => '',
        ]);

        if ($this->image != null){
            /*get original image name*/
            $getname= $this->image->getClientOriginalName();

            /*concatinate with a random time*/
            $imgname = time() . $getname;

            /*delete previous image*/
            $getimg = Storage::exists('public/products/' . $product->image);
            if ($getimg){
                Storage::delete('public/products/' . $product->image );
            }

            /*store new image*/
            $this->image->storeAs('public/products', $imgname);

            $product->update([
                'image' => $imgname,
            ]);

            /* Resizing Image with intervention image */
            Image::make('storage/products/'.$imgname)->resize(100, 100)->save();
    
        }
        
        $product->update([
            'name' => $this->name,
            'quantity' => $this->quantity,
            'category_id' => $this->category,
            'subcategory_id' => $this->subcategory,
            'supplier_id' => $this->supplier,
            'price' => $this->price,
            'description' => $this->description,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        $this->resetErrorBag();
        session()->flash('message', 'Update successfully.');
        $this->emitTo('products', 'update');
    }

    public function updatedImage(){
        $this->reset('view_image');
    }


    public function updatedCategory(){
        $subcategory = category::find($this->category)->subcategory()->get();
        $this->subcategories = $subcategory;
    }

    public function render()
    {
        $category = category::where('status', '1')->get();

        $supplier = supplier::where('status', '1')->get();

        return view('livewire.edit-product', [
            'categories' => $category,
            'suppliers' => $supplier,
        ]);
    }
}
