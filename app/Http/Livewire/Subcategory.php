<?php

namespace App\Http\Livewire;

use App\Models\category;
use Livewire\WithPagination;
use Livewire\Component;

class Subcategory extends Component
{
    use WithPagination;

    public $createsubcategory = false;
    public $deletesubcategory = false;
    public $delete_id;

    public $checkall ;
    public $check = [];

    public $search ;
    public $name = '';
    public $category = [];
    public $description = '';
    public $status = 1 ;

    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15;

    protected function getListeners()
    {
        return ['update' => 'subcategory_update'];
    }

    public function mount(){

    }

    public function show_subcategory_modal(){
        $this->createsubcategory = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deletesubcategory = true;
    }

    public function close_subcategory_modal(){
        $this->reset(['name', 'category', 'description', 'status']);
        $this->resetErrorBag();
        $this->createsubcategory = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deletesubcategory = false;
    }

    public  function createsubcategory(){
        $this->validate([
            'name' => 'required|max:255',
            'category' => 'required',
            'description' => 'required|max:255',
            'status' => '',
        ]);

        $data = \App\Models\subcategory::create([
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        $data->category()->attach($this->category);
        $this->reset(['name', 'category', 'description', 'status']);
        session()->flash('message', 'Sub-Category successfully Created.');

    }

    public function delete_subcategory($id){
        $this->deletesubcategory = false;
        \App\Models\subcategory::FindorFail($id)->delete();
        $this->reset('delete_id');
        session()->flash('del_message', 'Sub-Category successfully Deleted.');
    }


    public function multi_delete(){
        dd($this->check);
    }

    public function subcategory_update(){
        $this->render();
    }

    public function updatingSearch(){
        $this->resetPage();
    }

    public function render()
    {
        $category = category::where('status', '1')->get();
        $subcategory = \App\Models\subcategory::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);

        return view('livewire.subcategory', [
            'categories' => $category,
            'subcategories' => $subcategory,
        ]);
    }
}
