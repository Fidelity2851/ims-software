<?php

namespace App\Http\Livewire;


use App\Models\roles;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Livewire\Component;

class Workers extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $createworker = false;
    public $deleteworker = false;
    public $delete_id;

    public $name = '';
    public $role ;
    public $email ;
    public $date ;
    public $phone ;
    public $image ;
    public $password ;
    public $bio = '';
    public $status = 1 ;

    public $search ;
    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15 ;


    protected function getListeners()
    {
        return ['update' => 'worker_update'];
    }

    public function mount(){

    }


    public function show_worker_modal(){
        $this->createworker = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deleteworker = true;
    }

    public function close_worker_modal(){
        $this->reset([]);
        $this->resetErrorBag();
        $this->createworker = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deleteworker = false;
    }


    public function remove_image(){
        $this->reset('image');
    }

    public function create_workers(){
        $this->validate([
            'name' => 'required|unique:users|max:255',
            'role' => 'required|integer',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'date' => 'required|date',
            'image' => 'required|file|image|max:2048',
            'password' => 'required',
            'bio' => 'required|max:255',
            'status' => '',
        ]);

        /*get original image name*/
        $getname= $this->image->getClientOriginalName();

        /*concatinate with a random time*/
        $imgname = time() . $getname;
        $this->image->storeAs('public/workers', $imgname);
        
        /* Resizing Image with intervention image */
        Image::make('storage/workers/'.$imgname)->resize(200, 200)->save();

        User::create([
            'name' => $this->name,
            'role_id' => $this->role,
            'email' => $this->email,
            'phone' => $this->phone,
            'dob' => $this->date,
            'password' => Hash::make($this->password),
            'image' => $imgname,
            'bio' => $this->bio,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        $this->reset(['name', 'role', 'email', 'phone', 'date', 'password', 'image', 'bio', 'status']);
        session()->flash('message', 'Worker successfully Created.');
    }

    public function delete_worker(User $user){
        $getimg = Storage::exists('public/workers/' . $user->image);
        $user->delete();
        if ($getimg){
            Storage::delete('public/workers/' . $user->image );
        }

        $this->reset('delete_id');
        $this->deleteworker = false;
        session()->flash('del_message', 'Worker successfully Deleted.');
    }

    public function multi_delete(){

        dd($this->check);

    }


    public function worker_update(){
        $this->render();
    }

    public function render()
    {
        $role = roles::where('status', '1')->get();
        $worker = User::Search($this->search)->with('roles')->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);

        return view('livewire.workers', [
            'workers' => $worker,
            'roles' => $role,
        ]);
    }
}
