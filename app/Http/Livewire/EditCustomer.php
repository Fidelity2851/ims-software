<?php

namespace App\Http\Livewire;

use App\Models\customer;
use Livewire\Component;

class EditCustomer extends Component
{
    public $editcustomer = false;

    public $customer_id = null;
    public $name ;
    public $phone ;
    public $email ;
    public $address ;
    public $status ;


    protected function getListeners()
    {
        return ['show_modal' => 'show_editcustomer_modal'];
    }

    public function mount(){

    }


    public function show_editcustomer_modal(customer $customer){
        $this->customer_id = $customer->id;
        $this->editcustomer = true;
        $this->name = $customer->name;
        $this->phone = $customer->phone;
        $this->email = $customer->email;
        $this->address = $customer->address;
        if ($customer->status == 1){
            $this->status = true;
        }
        else{
            $this->status = false;
        }

    }

    public function close_editcustomer_modal(){
        $this->reset(['customer_id', 'name', 'phone', 'email', 'address', 'status']);
        $this->resetErrorBag();
        $this->editcustomer = false;
    }

    public function updatecustomer(customer $customer){
        $this->validate([
            'name' => 'required|max:255',
            'phone' => 'required|integer',
            'email' => 'required|email',
            'address' => 'required|max:255',
            'status' => '',
        ]);

        $customer->update([
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->address,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        /*$this->reset(['name', 'description', 'status']);*/
        $this->resetErrorBag();
        session()->flash('message', 'Updated successfully.');
        $this->emitTo('suppliers', 'update');
    }


    public function render()
    {
        return view('livewire.edit-customer');
    }
}
