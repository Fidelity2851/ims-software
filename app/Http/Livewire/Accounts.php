<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\account;
use Illuminate\Validation\Rule;
use Livewire\Component;

class Accounts extends Component
{
    public $payment = false;
    public $delete_payment = false;
    public $view_payment = false;
    public $delete_id ;
    public $update_id ;

    public $date ;
    public $month ;
    public $track ;
    public $workers ;
    public $total ;
    public $status ;
    public $created_at ;
    
    public $search ;
    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15 ;


    public function mount(){
        
    }
    
    public function generate_payment(){
        $this->payment = true;
        $this->track = time();

        $this->workers = User::where('status', '1')->has('roles')->get();

        foreach ($this->workers as $value) {
            $this->total = $value->roles->sum('salary');
        }
        
    }
    public function close_payment(){
        $this->reset('track', 'date', 'workers', 'month', 'total', 'status');
        $this->resetErrorBag();
        $this->payment = false;
    }

    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->delete_payment = true;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->delete_payment = false;
    }

    public function view_account(account $account){
        $this->view_payment = true;

        $this->update_id = $account->id;
        $this->track = $account->payment_number;
        $this->date = $account->month;
        $this->month = date('F, Y', strtotime($account->month));
        $this->status = $account->status == 1 ? true : false;
        $this->created_at = $account->created_at->diffForHumans();
        $this->workers = $account->user()->get();
        foreach ($this->workers as $value) {
            $this->total = $value->roles->sum('salary');
        }
        
    }
    public function close_view_account(){
        $this->view_payment = false;

        $this->reset('update_id', 'track', 'date', 'workers', 'month', 'total', 'status', 'created_at');
        $this->resetErrorBag();
    }


    public function process(){
        $this->validate([
            'date' => ['required', 'date', Rule::unique('accounts', 'month')],
        ]);

        $data = account::create([
            'payment_number' => $this->track,
            'month' => $this->date,
            'status' => $this->status ? 1 : 0 ,
        ]);
        foreach ($this->workers as $value) {
            $data->user()->attach($value->id, ['salary' => $value->roles->salary]);
        }

        session()->flash('message', 'Payment Successfully Created.');
    }
    public function delete_payment(account $account){
        $account->delete();
        $this->delete_payment = false;

        $this->reset('delete_id');
        session()->flash('del_message', 'Account Successfully Deleted.');
    }
    
    public function update_account(account $account){
        $this->validate([
            'date' => ['sometimes' , Rule::unique('accounts', 'month')->ignore($account->id)],
        ]);

        $account->update([
            'status' => $this->status,
        ]);

        if ($this->date != null) {
            $account->update([
                'month' => $this->date,
            ]);
        }

        session()->flash('message', 'Account Successfully Updated.');
    }

    public function updatedDate(){
        $this->month = date('F, Y', strtotime($this->date));
    }
    
    public function render()
    {
        $account = account::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);
        
        return view('livewire.accounts', [
            'accounts' => $account,
        ]);
    }
}
