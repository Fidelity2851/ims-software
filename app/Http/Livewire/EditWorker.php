<?php

namespace App\Http\Livewire;

use App\Models\roles;
use App\Models\User;
use App\Models\worker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Livewire\WithFileUploads;
use Intervention\Image\Facades\Image;
use Livewire\Component;

class EditWorker extends Component
{
    use WithFileUploads;

    public $editworker = false;
    public $view_image ;

    public $worker_id = null;

    public $name = '';
    public $role ;
    public $email ;
    public $date ;
    public $phone ;
    public $image ;
    public $password ;
    public $bio = '';
    public $status = 1 ;


    protected $rules = [
        
    ];

    protected function getListeners()
    {
        return ['show_modal' => 'show_editproducts_modal'];
    }

    public function mount(){

    }


    public function show_editproducts_modal(User $worker){
        $this->editworker = true;
        $this->worker_id = $worker->id;

        $this->name = $worker->name;
        $this->role = $worker->roles->id;
        $this->email = $worker->email;
        $this->phone = $worker->phone;
        $this->date = $worker->dob;
        $this->view_image = $worker->image;
        $this->bio = $worker->bio;
        if ($worker->status == 1){
            $this->status = true;
        }
        else{
            $this->status = false;
        }

    }

    public function close_editworker_modal(){
        $this->editworker = false;
        $this->reset(['name', 'role', 'email', 'phone', 'date', 'password', 'image', 'bio', 'status']);
        $this->resetErrorBag();
    }

    public function remove_image(){
        $this->reset('image');
    }


    public function update_worker(User $worker){
        $this->validate([
            'name' => ['required', Rule::unique('users', 'name')->ignore($worker->id), 'max:255'],
            'role' => 'required|integer',
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($worker->id), 'max:255'],
            'phone' => 'required',
            'date' => 'required|date',
            'image' => 'sometimes|max:2048',
            'password' => 'sometimes',
            'bio' => 'required|max:255',
            'status' => '',
        ]);

        if ($this->image != null){
            /*get original image name*/
            $getname= $this->image->getClientOriginalName();

            /*concatinate with a random time*/
            $imgname = time() . $getname;

            /*delete previous image*/
            $getimg = Storage::exists('public/workers/' . $worker->image);
            if ($getimg){
                Storage::delete('public/workers/' . $worker->image );
            }
            /*store new image*/
            $this->image->storeAs('public/workers', $imgname);

            $worker->update([
                'image' => $imgname,
            ]);

            /* Resizing Image with intervention image */
            Image::make('storage/workers/'.$imgname)->resize(200, 200)->save();

        }

        $worker->update([
            'name' => $this->name,
            'roles_id' => $this->role,
            'email' => $this->email,
            'phone' => $this->phone,
            'dob' => $this->date,
            'bio' => $this->bio,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        if($this->password){
            $worker->update([
                'password' => Hash::make($this->password),
            ]);
        }

        session()->flash('message', 'Updated successfully.');
        $this->emitTo('workers', 'update');
    }

    public function updatedImage(){
        $this->reset('view_image');
    }


    public function render()
    {
        $role = roles::where('status', '1')->get();

        return view('livewire.edit-worker', [
            'roles' => $role,
        ]);
    }
}
