<?php

namespace App\Http\Livewire;

use App\Models\category;
use Livewire\Component;

class EditCategory extends Component
{

    public $editcategory = false;

    public $category_id = null;
    public $name ;
    public $description ;
    public $status ;


    protected function getListeners()
    {
        return ['show_modal' => 'show_editcategory_modal'];
    }

    public function mount(){

    }


    public function show_editcategory_modal(category $category){
        $this->category_id = $category->id;
        $this->editcategory = true;
        $this->name = $category->name;
        $this->description = $category->description;
        if ($category->status == 1){
            $this->status = true;
        }
        else{
            $this->status = false;
        }

    }

    public function close_editcategory_modal(){
        $this->reset(['category_id','name', 'description', 'status']);
        $this->resetErrorBag();
        $this->editcategory = false;
    }


    public function updatecategory(category $category){
        $this->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'status' => '',
        ]);


        $category->update([
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status != null ? $this->status : '0',
        ]);
        /*$this->reset(['name', 'description', 'status']);*/
        $this->resetErrorBag();
        session()->flash('message', 'Update successfully.');
        $this->emitTo('category', 'update');
    }


    public function render()
    {
        return view('livewire.edit-category');
    }
}
