<?php

namespace App\Http\Livewire;

use App\Models\customer;
use App\Models\mail;
use App\Models\supplier;
use http\Message\Body;
use Livewire\WithPagination;
use App\Mail\MessageMail;
use Illuminate\Support\Facades\Mail as SendMail;
use Livewire\Component;

class Mails extends Component
{
    use WithPagination;

    public $createmail = false;
    public $deletemail = false;
    public $viewmail = false;
    public $delete_id;

    public $checkall ;
    public $check = [];
    public $category = [];

    public $search ;
    public $customer = [];
    public $supplier = [];
    public $subject ;
    public $body = '';
    public $status = 1 ;

    public $mailcustomer  ;
    public $mailsupplier ;
    public $mailmessage ;
    public $mailsubject ;

    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15;



    public function mount(){

    }


    public function show_mail_modal(){
        $this->createmail = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deletemail = true;
    }

    public function close_mail_modal(){
        $this->reset(['customer', 'supplier', 'subject', 'body', 'status']);
        $this->resetErrorBag();
        $this->createmail = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deletemail = false;
    }

    public  function createmail(){
        $this->validate([
            'customer' => 'required_without:supplier|max:255',
            'supplier' => 'required_without:customer|max:255',
            'subject' => 'required',
            'body' => 'required',
            'status' => '',
        ]);

        $msg = mail::create([
            'subject' => $this->subject,
            'body' => $this->body,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        if ($this->customer != []){
            foreach ($this->customer as $cus){
                $msg->customer()->attach($cus);
                // Send Mail
                $customer =  customer::findorfail($cus);
                $this->send_mail($customer, $this->subject, $this->body);
            }
        }
        if ($this->supplier != []){
            foreach ($this->supplier as $sup){
                $msg->supplier()->attach($sup);
                // Send Mail
                $supplier =  supplier::findorfail($sup);
                $this->send_mail($supplier, $this->subject, $this->body);
            }
        }

        $this->reset(['customer', 'supplier', 'subject', 'body', 'status']);
        session()->flash('message', 'Mail Sent successfully.');


    }

    public function delete_mail($id){
        $this->deletemail = false;
        mail::FindorFail($id)->delete();
        $this->reset('delete_id');
        session()->flash('del_message', 'Mail successfully Deleted.');
    }
    public function multiple_delete(){
        $mails = mail::whereIn('id', $this->check)->delete();

        $this->reset('check');
        session()->flash('del_message', 'Mail successfully Deleted.');
    }

    public function view_mail(mail $mail){
        $this->viewmail = true;
        $this->mailcustomer = $mail->customer;
        $this->mailsupplier = $mail->supplier;
        $this->mailmessage = $mail->body;
        $this->mailsubject = $mail->subject;
    }

    public function close_mail(){
        $this->reset(['mailcustomer', 'mailsupplier', 'mailmessage', 'mailsubject']);
        $this->viewmail = false;
    }

    public function updatingSearch(){
        $this->resetPage();
    }

    // Sending Mails
    public function send_mail($user, $subject, $message){
        SendMail::to($user->email)->send(new MessageMail($user, $subject, $message));
    }
    
    public function render()
    {
        $customer = customer::where('status', '1')->get();
        $supplier = supplier::where('status', '1')->get();
        $mail = mail::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);

        return view('livewire.mails', [
            'customers' => $customer,
            'suppliers' => $supplier,
            'mails' => $mail,
        ]);
    }
}
