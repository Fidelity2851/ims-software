<?php

namespace App\Http\Livewire;


use Livewire\WithPagination;
use Livewire\Component;

class Roles extends Component
{
    use WithPagination;

    public $createrole = false;
    public $deleterole = false;
    public $delete_id;

    public $search ;
    public $name ;
    public $salary ;
    public $description ;
    public $status = 1 ;

    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15;


    protected function getListeners()
    {
        return ['update' => 'role_update'];
    }


    public function mount(){

    }


    public function show_role_modal(){
        $this->createrole = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deleterole = true;
    }

    public function close_role_modal(){
        $this->reset(['name', 'salary', 'description', 'status']);
        $this->resetErrorBag();
        $this->createrole = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deleterole = false;
    }


    public  function create_role(){
        $this->validate([
            'name' => 'required|unique:roles|max:255',
            'salary' => 'required|integer',
            'description' => 'required|max:255',
            'status' => '',
        ]);

        \App\Models\roles::create([
            'name' => $this->name,
            'salary' => $this->salary,
            'description' => $this->description,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        $this->reset(['name', 'salary', 'description', 'status']);
        session()->flash('message', 'Role successfully Created.');


    }

    public function delete_role($id){
        \App\Models\roles::FindorFail($id)->delete();
        $this->reset('delete_id');
        $this->deleterole = false;
        session()->flash('del_message', 'Workers successfully Deleted.');
    }


    public function multi_delete(){
        dd($this->check);
    }


    public function role_update(){
        $this->render();
    }

    public function updatingSearch(){
        $this->resetPage();
    }


    public function render()
    {
        $role = \App\Models\roles::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);

        return view('livewire.roles', [
            'roles' => $role,
        ]);
    }
}
