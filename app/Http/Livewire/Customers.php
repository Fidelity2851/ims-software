<?php

namespace App\Http\Livewire;

use App\Models\customer;
use Livewire\Component;
use Livewire\WithPagination;

class Customers extends Component
{
    use WithPagination;

    public $createcustomer = false;
    public $deletecustomer = false;
    public $delete_id;

    public $search ;

    public $name ;
    public $phone ;
    public $email ;
    public $address ;
    public $status = 1 ;

    public $OrderBy = 'id';
    public $OrderAsc = 0;
    public $PerPage = 15 ;


    protected function getListeners()
    {
        return ['update' => 'supplier_update'];
    }


    public function mount(){

    }


    public function show_customer_modal(){
        $this->createcustomer = true;
    }
    public function show_delete_modal($id){
        $this->delete_id = $id;
        $this->deletecustomer = true;
    }

    public function close_customer_modal(){
        $this->reset(['name', 'phone', 'email', 'address', 'status']);
        $this->resetErrorBag();
        $this->createcustomer = false;
    }
    public function close_delete_modal(){
        $this->reset('delete_id');
        $this->deletecustomer = false;
    }

    public  function createcustomer(){
        $this->validate([
            'name' => 'required|max:255',
            'phone' => 'required|integer',
            'email' => 'required|email|unique:customers',
            'address' => 'required|max:255',
            'status' => '',
        ]);

        customer::create([
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->address,
            'status' => $this->status != null ? $this->status : '0',
        ]);

        $this->reset(['name', 'phone', 'email', 'address', 'status']);
        session()->flash('message', 'Customers successfully Created.');


    }

    public function delete_customer($id){
        $this->deletecustomer = false;
        customer::FindorFail($id)->delete();
        $this->reset('delete_id');
        session()->flash('del_message', 'Customer successfully Delete.');
    }


    public function multi_delete(){

        dd($this->check);

    }

    public function supplier_update(){
        $this->render();
    }

    public function updatingSearch(){
        $this->resetPage();
    }


    public function render()
    {
        $customer =   customer::Search($this->search)->orderBy($this->OrderBy, $this->OrderAsc ? 'asc' : 'desc')->paginate($this->PerPage);

        return view('livewire.customers', [
            'customers' => $customer,
        ]);
    }
}
