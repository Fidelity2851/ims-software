<?php

namespace App\Http\Livewire;

use App\Models\customer;
use App\Models\product;
use Livewire\Component;

class Pos extends Component
{
    public $order = false;
    public $checklist = [];

    public $search ;
    public $items = [];

    public $price ;
    public $subtotal = 0;
    public $total = 0;
    public $qty = 1;

    public $selectedcustomer ;


    public function mount(){

    }

    public function add($id){
        $product = product::find($id)->toArray();
        $this->subtotal += $product['price'];
        $this->total += $product['price'];
        array_push($this->items, $product);    

    }
    public function remove($key, $item){
        
        array_splice($this->items, $key, 1);
        $this->subtotal -= $item;
        $this->total -= $item;
    }

    public function increase(){

    }
    public function descrease(){
        
    }

    public function checkout(){
        $this->order = true;
    }
    public function close_order(){
        $this->order = false;
    }

    public function render()
    {
        $customer = customer::where('status', '1')->get();
        $product = product::where('name', 'like', '%'.$this->search.'%')->where('status', '1')->get();

        return view('livewire.pos', [
            'products' => $product,
            'customers' => $customer,
        ]);
    }
}
