@component('mail::message')
# {{ $subject }}

<strong>Name :</strong> {{ $user->name}}
<br>
<strong>Email Address :</strong> {{ $user->email}}

{{ $message}}
 

@component('mail::button', ['url' => ''])
Welcome
@endcomponent

Thanks,<br>
Inventory Management System.
@endcomponent
