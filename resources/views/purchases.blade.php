@extends('layouts.layout')

@section('title', 'Purchases')

@section('content')
    <livewire:purchases />
@endsection
