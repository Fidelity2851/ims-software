<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>POS - Login</title>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800&family=Overpass:wght@200;300;400;600;700;800&display=swap" rel="stylesheet">

    <!--font awesome-->
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <!-- Styleshheet -->
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

    <!--icons-->
    <link href="{{ asset('css/metro-icons.css') }}" rel="stylesheet">
</head>
<body>
<!--housing DIV-->
<div class="row housing mx-0">
    <div class="home_con d-flex justify-content-center">
        <div class="col col-md-10 col-lg-8 home_cont_con align-self-center px-3">
            <h1 class="home_header mb-5">Admin Login</h1>

            <div class="col col-md-10 col-lg-10 mx-auto">
                <x-jet-validation-errors class="mb-4 home_cont_text" />
                <form action="{{ route('login') }}" method="post" class="col col-md-4 mx-auto px-0">
                    @csrf
                    <div class="mb-3">
                        <input id="email" type="email" class="home_box" name="email" :value="old('email')" placeholder="Email" data-role="input" data-prepend="<span class='mif-user'></span>" required autofocus>
                    </div>
                    <div class="mb-3">
                        <input id="password" type="password" class="home_box" type="password" name="password" placeholder="Password" data-role="input" data-prepend="<span class='mif-lock'></span>" required autocomplete="current-password">
                    </div>
                    <button type="submit" class="home_btn">Process</button>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Javascript Files -->
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/popper.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/index.js') }}"></script>

</body>
</html>
