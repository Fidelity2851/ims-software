<!DOCTYPE html>
<html lang="en" xmlns:livewire="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>POS - Point of sales</title>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800&family=Overpass:wght@200;300;400;600;700;800&display=swap" rel="stylesheet">

    <!--font awesome-->
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <!-- Styleshheet -->
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

    <!--icons-->
    <link href="{{ asset('css/metro-icons.css') }}" rel="stylesheet">

    {{--livewire--}}
    <livewire:styles />
</head>
<body>

<!--housing DIV-->
<div class="housing ">
    <livewire:pos />
</div>

<!-- Javascript Files -->
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/popper.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/index.js') }}"></script>

{{--livewire--}}
<livewire:scripts />
</body>
</html>
