@extends('layouts.layout')

@section('title', 'Messages')

@section('content')
    <livewire:messages />
@endsection
