<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>POS Software</title>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800&family=Overpass:wght@200;300;400;600;700;800&display=swap" rel="stylesheet">

    <!--font awesome-->
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <!-- Styleshheet -->
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/clock.css') }}" rel="stylesheet">

    <!--icons-->
    <link href="{{ asset('css/metro-icons.css') }}" rel="stylesheet">
</head>
<body>
<!--housing DIV-->
<div class="row housing mx-0">
    <div class="home_con d-flex justify-content-center">
        <div class="col col-md-10 col-lg-8 home_cont_con align-self-center">
            <h1 class="home_header mb-5">Welcome To POS Software</h1>
            <div class="">
                <p class="home_cont_head">Work starts by <span class="home_cont_head_col"> 8:00am</span> </p>
                <p class="home_cont_text">You are 20 minutes Late</p>
            </div>
            <div class="col col-md-10 col-lg-6 mx-auto mb-5 px-3">
                <div id="clock" class="col light p-0">
                    <div class="col display mx-auto ">
                        <div class="weekdays"></div>
                        <div class="ampm"></div>
                        <div class="alarm"></div>
                        <div class="digits"></div>
                    </div>
                </div>
            </div>
            <div class="">
                @if(session()->has('msg'))
                    <p class="home_cont_text ">{{ session()->get('msg') }}</p>
                @endif
                <form action="{{ route('clock') }}" method="post" class="col col-md-4 mx-auto px-3">
                    @csrf
                    <div class="mb-3">
                        <input type="password" class="home_box form-control" name="password" placeholder="Enter Your ID">
                    </div>
                    <button type="submit" class="home_btn">Process</button>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Javascript Files -->
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/clock.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/popper.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.0.0/moment.min.js"></script>
</body>
</html>
