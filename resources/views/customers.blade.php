@extends('layouts.layout')

@section('title', 'Customers')

@section('content')
    <livewire:customers />
@endsection
