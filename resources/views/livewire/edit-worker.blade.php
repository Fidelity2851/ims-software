<div xmlns:wire="http://www.w3.org/1999/xhtml">

@if($editworker)
    {{--Edit Workers modal--}}
    <div class="modal d-block modal_con" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="f_head">Edit Workers</p>
                    <button wire:click="close_editworker_modal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form wire:submit.prevent="update_worker({{ $worker_id }})">
                    @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong class="table_msg">Successfully! {{ session('message') }} </strong>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="d-md-flex ">
                            <div class="col mb-3 me-md-3">
                                <input wire:model.lazy="name" type="text" class="f_box" placeholder="Username" required>
                                @error('name') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col mb-3">
                                <select wire:model="role" class="f_sel" required>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @error('role') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="d-md-flex ">
                            <div class="col mb-3 me-md-3">
                                <input wire:model.lazy="email" type="email" class="f_box" placeholder="Email Address" required>
                                @error('email') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col mb-3">
                                <input wire:model.lazy="phone" type="tel" class="f_box" placeholder="Phone" required>
                                @error('phone') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="d-md-flex ">
                            <div class="col mb-3 me-md-3">
                                <input wire:model.lazy="date" type="date" class="f_box" placeholder="Date of Birth" required>
                                @error('date') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col mb-3">
                                <input wire:model.lazy="password" type="password" class="f_box" placeholder="Password" >
                                @error('password') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col mb-3 ">
                            <input wire:model.lazy="image" type="file" class="f_box">
                            @error('image') <span class="error">{{ $message }}</span> @enderror
                            <div class="" wire:loading wire:target="image">
                                <div class="spinner-grow spinner-grow-sm text-primary" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>
                            @if($view_image)
                                <div class="">
                                    <img src="{{ 'storage/workers/' . $view_image }}" class="f_img">
                                    <p wire:click="remove_image" class="f_img_text mb-0">Remove</p>
                                </div>
                            @endif
                            @if($image)
                                <div class="">
                                    <img src="{{ $image->temporaryUrl() }}" class="f_img">
                                    <p wire:click="remove_image" class="f_img_text mb-0">Remove</p>
                                </div>
                            @endif
                        </div>
                        <div class="col ">
                            <textarea wire:model.lazy="bio" class="f_area" placeholder="Short Biograph" required></textarea>
                            @error('bio') <span class="error">{{ $message }}</span> @enderror
                            <div class="form-check form-switch">
                                <input id="check" wire:model="status" type="checkbox" class="form-check-input" checked>
                                <label for="check" class="form-check-label f_label">Active</label>
                            </div>
                        </div>
                    </div>
                    <div class="f_footer">
                        <button wire:loading.remove wire:target="update_worker" type="submit" class="f_btn">Update</button>
                        <button wire:loading wire:target="update_worker" type="button" class="f_btn" disabled>
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Updating...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif

</div>
