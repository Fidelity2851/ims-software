<div class="" xmlns:wire="http://www.w3.org/1999/xhtml">
@if($editrole)
    <div class="modal d-block modal_con" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="f_head">Edit Roles</p>
                    <button wire:click="close_editrole_modal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form wire:submit.prevent="update_role({{ $role_id }})">
                    @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong class="table_msg">Successfully! {{ session('message') }} </strong>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="d-md-flex ">
                            <div class="col mb-3 me-md-3">
                                <input wire:model.lazy="name" type="text" class="f_box" data-role="input" placeholder="Name" required>
                                @error('name') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col d-flex mb-3">
                                <span class="f_icon">$</span>
                                <input wire:model.lazy="salary" type="number" class="f_box" placeholder="Salary" required>
                                @error('salary') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col ">
                            <textarea wire:model.lazy="description" class="f_area" placeholder="Description" required></textarea>
                            @error('description') <span class="error">{{ $message }}</span> @enderror
                            <div class="form-check form-switch">
                                <input id="check" wire:model="status" type="checkbox" class="form-check-input" checked>
                                <label for="check" class="form-check-label f_label">Active</label>
                            </div>
                        </div>
                    </div>
                    <div class="f_footer">
                        <button wire:loading.remove wire:target="update_role" type="submit" class="f_btn">Update</button>
                        <button wire:loading wire:target="update_role" type="button" class="f_btn" disabled>
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Updating...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif

</div>
