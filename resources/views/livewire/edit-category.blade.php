<div class="" xmlns:wire="http://www.w3.org/1999/xhtml">

@if($this->editcategory)
    <!-- Edit Category modal -->
    <div class="modal d-block modal_con" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="f_head">Edit Category</p>
                    <button wire:click="close_editcategory_modal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form wire:submit.prevent="updatecategory({{$category_id}})">
                    @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong class="table_msg">Successfully! {{ session('message') }} </strong>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="col mb-3">
                            <input wire:model.lazy="name" type="text" class="f_box" data-role="input" placeholder="Name" required>
                            @error('name') <span class="error">{{ $message }}</span> @enderror
                        </div>
                        <div class="col ">
                            <textarea wire:model.lazy="description" class="f_area" placeholder="Description" required></textarea>
                            @error('description') <span class="error">{{ $message }}</span> @enderror
                            <div class="form-check form-switch">
                                <input id="check" wire:model="status" type="checkbox" class="form-check-input" >
                                <label for="check" class="form-check-label f_label">Active</label>
                            </div>
                        </div>
                    </div>
                    <div class="f_footer">
                        <button wire:loading.remove wire:target="updatecategory" type="submit" class="f_btn">Update</button>
                        <button wire:loading wire:target="updatecategory" type="button" class="f_btn" disabled>
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Updating...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif

</div>
