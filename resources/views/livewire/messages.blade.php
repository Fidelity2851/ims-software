<div class="col nav_cont_con px-3" xmlns:wire="http://www.w3.org/1999/xhtml">

@if($createmessage)
    <!--Create Message modal -->
        <div class="modal d-block modal_con" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="f_head">Send Mail</p>
                        <button wire:click="close_message_modal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form wire:submit.prevent="createmessage">
                        @if (session()->has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                <strong class="table_msg">Successfully! {{ session('message') }} </strong>
                            </div>
                        @endif
                        <div class="modal-body">
                            <div class="d-md-flex ">
                                <div class="col mb-3">
                                    <select wire:model="worker" class="f_sel" multiple>
                                        @foreach($workers as $worker)
                                            <option value="{{ $worker->id }}">{{ $worker->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('worker') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            <div class="col ">
                                <textarea wire:model.lazy="body" class="f_area" placeholder="Message" required></textarea>
                                @error('body') <span class="error">{{ $message }}</span> @enderror
                                <div class="form-check form-switch">
                                    <input id="check" wire:model="status" type="checkbox" class="form-check-input" checked>
                                    <label for="check" class="form-check-label f_label">Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="f_footer">
                            <button wire:loading.remove wire:target="createmessage" type="submit" class="f_btn">Create</button>
                            <button wire:loading wire:target="createmessage" type="button" class="f_btn" disabled>
                                <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                Processing...
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@elseif($deletemessage)
    <!--Delete Message modal -->
        <div class="modal d-block modal_con" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="f_head">Confirm Deleting Suuplier</p>
                        <button wire:click="close_delete_modal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p class="f_label">Note: Deleting this customer will also delete all its relationships!</p>
                    </div>
                    <div class="f_footer mt-4">
                        <button wire:loading.remove wire:target="delete_message" wire:click="delete_message({{ $delete_id }})" type="button" class="f_btn">Confirm</button>
                        <button wire:loading wire:target="delete_message" type="button" class="f_btn" disabled>
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Processing...
                        </button>
                        <button wire:click="close_delete_modal" type="button" class="f_btn2 js-dialog-close">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
@elseif($viewmessage)
    <!--View Message-->
        <div class="modal d-block modal_con" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="f_head">View Message</p>
                        <button wire:click="close_message" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <p class="m_head">Workers:</p>
                            <p class="m_text">
                                @foreach($messageworker as $worker)
                                    {{ $worker->name }},
                                @endforeach
                            </p>
                        </div>
                        <div class="">
                            <p class="m_head">Message:</p>
                            <p class="m_text">{{ $message }}</p>
                        </div>
                    </div>
                    <div class="f_footer">
                        <button wire:click="close_message" type="submit" class="f_btn2">Close</button>
                    </div>
                </div>
            </div>
        </div>
@endif


    <div class="col d-flex flex-column-reverse flex-md-row justify-content-md-between align-items-center mb-4">
        <div class="">
            <button wire:click="show_message_modal" type="button" class="c_btn">Create</button>
        </div>
        <div class="bread_link_con ">
            <a href="dashboard.blade.php" class="text-decoration-none"><span class="bread_link">Dashboard / </span> </a>
            <span class="bread_link">Messages</span>
        </div>
    </div>
    <div class="">
        @if (session()->has('del_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong class="table_msg">Successfully! {{ session('del_message') }} </strong>
            </div>
        @endif
            <div class="col navbar navbar-expand-md d-flex justify-content-between align-items-center mb-3 ">
                <form class="col col-md-4 px-0-md me-3">
                    <input wire:model.debounce.500ms="search" type="search" class="table_box" placeholder="Search Suppliers...">
                </form>
                <button class="table_btn navbar-toggler border p-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="mif-filter"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end mt-3 mt-md-0" id="navbarSupportedContent">
                    <div class="d-flex overflow-auto px-md-0 ">
                        <select wire:model="OrderBy" class="table_sel" >
                            <option value="id">ID</option>
                            <option value="status">Status</option>
                        </select>
                        <select  wire:model="OrderAsc" class="table_sel ms-2 " >
                            <option value="1">Assending</option>
                            <option value="0">Desending</option>
                        </select>
                        <select  wire:model="PerPage" class="table_sel ms-2">
                            <option value="15">15</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-center align-self-center mb-2">
                <div class="" wire:loading wire:target="search">
                    <div class="spinner-grow text-primary" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                    <div class="spinner-grow text-danger" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                    <div class="spinner-grow text-warning" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
                @if($messages->count() < 1)
                    <p class="t_res " wire:loading.remove wire:target="search" >No Record Found...</p>
                @endif
            </div>

        <!--Table-->
            <div class="table-responsive-md">
                <table class="table table-striped mb-4">
                    <thead class="t_head_con">
                    <tr>
                        <th class="t_head">#</th>
                        <th class="t_head">
                            <input id="main_check" type="checkbox" class="form-check-input">
                        </th>
                        <th class="t_head">Workers</th>
                        <th class="t_head">Message</th>
                        <th class="t_head">Status</th>
                        <th class="t_head">Manage</th>
                    </tr>
                    </thead>
                    <tbody >
                    @foreach($messages as $message)
                        <tr>
                            <td class="t_data">{{ $loop->iteration }}</td>
                            <td>
                                <input type="checkbox" wire:model="check" value="{{ $message->id }}"  class="form-check-input">
                            </td>
                            <td class="t_data ">
                                @foreach($message->user as $worker)
                                    {{ $worker->name }},
                                @endforeach
                            </td>
                            <td class="t_data">{{ $message->body }}</td>
                            <td>
                                @if($message->status == 1)
                                    <span class="t_status1">Active</span>
                                @else
                                    <span class="t_status2">pending</span>
                                @endif
                            </td>
                            <td class="">
                                <span wire:click="view_message({{ $message->id }})" title="Edit" class="mif-eye t_icon "></span>
                                <span wire:click="show_delete_modal({{ $message->id }})" title="Delete" class="mif-bin t_icon2"></span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="d-md-flex justify-content-between align-items-end">
                <div class="d-flex mb-3 mb-md-0">
                    <button wire:click="multi_delete_category" type="button" class="t_btn_del flex-self-center me-2">Delete</button>
                    <button type="button" class="t_btn_pen flex-self-center me-2">Pending</button>
                    <button type="button" class="t_btn_app flex-self-center">Approve</button>
                </div>
                {{ $messages->links('livewire.pagination-links') }}
            </div>
    </div>
</div>
