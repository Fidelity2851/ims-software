<div class="" xmlns:wire="http://www.w3.org/1999/xhtml">

    @if($editproduct)
        <!--Edit Products modal -->
        <div class="modal d-block modal_con" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <p class="f_head">Edit Products</p>
                            <button wire:click="close_editproducts_modal" type="button" class="btn-close" ></button>
                        </div>
                        <form wire:submit.prevent="update_products({{$product_id}})">
                            @if (session()->has('message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    <strong class="table_msg">Successfully! {{ session('message') }} </strong>
                                </div>
                            @endif
                            <div class="modal-body">
                                <div class="d-md-flex ">
                                    <div class="col mb-3 me-md-3">
                                        <input wire:model.lazy="name" type="text" class="f_box" placeholder="Name" required>
                                        @error('name') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col mb-3 ">
                                        <input wire:model.lazy="quantity" type="number" class="f_box" placeholder="Quantity" required>
                                        @error('quantity') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="d-md-flex ">
                                    <div class="col mb-3 me-md-3">
                                        <select wire:model="category" class="f_sel" required>
                                        
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('category') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col mb-3 me-md-3">
                                        <select wire:model="subcategory" class="f_sel" required>
                                            @foreach($subcategories as $subcategory)
                                                <option value="{{$subcategory->id}}">{{$subcategory->name}}</option>
                                            @endforeach
                                            @empty($subcategories)
                                                <option selected>No Sub Category Found</option>
                                            @endempty
                                        </select>
                                        @error('subcategory') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col mb-3 ">
                                        <select wire:model="supplier" class="f_sel" required>
                                            @foreach($suppliers as $supplier)
                                                <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('supplier') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="d-md-flex ">
                                    <div class="col mb-3 me-md-3">
                                        <input wire:model="image" type="file" class="f_box" placeholder="image" >
                                        @error('image') <span class="error">{{ $message }}</span> @enderror
                                        <div class="" wire:loading wire:target="image">
                                            <div class="spinner-grow spinner-grow-sm text-primary" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                        </div>
                                        @if($view_image)
                                            <div class="">
                                                <img src="{{ asset('storage/products/' . $view_image) }}" class="f_img">
                                                <p wire:click="remove_image" class="f_img_text mb-0">Remove</p>
                                            </div>
                                        @endif
                                        @if($image)
                                            <div class="">
                                                <img src="{{ $image->temporaryUrl() }}" class="f_img">
                                                <p wire:click="remove_image" class="f_img_text mb-0">Remove</p>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col mb-3 ">
                                    <input wire:model.lazy="price" type="number" class="f_box" placeholder="Price" required>
                                    @error('price') <span class="error">{{ $message }}</span> @enderror
                                </div>
                                </div>
                                <div class="col ">
                                    <textarea wire:model.lazy="description" class="f_area" placeholder="Description" required></textarea>
                                    @error('description') <span class="error">{{ $message }}</span> @enderror
                                    <div class="form-check form-switch">
                                        <input id="check" wire:model="status" type="checkbox" class="form-check-input" checked>
                                        <label for="check" class="form-check-label f_label">Active</label>
                                    </div>
                                </div>
                            </div>
                            <div class="f_footer">
                                <button wire:loading.remove wire:target="update_products" type="submit" class="f_btn">Update</button>
                                <button wire:loading wire:target="update_products" type="button" class="f_btn" disabled>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    Updating...
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    @endif

</div>
