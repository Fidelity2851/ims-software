<div class="d-xl-flex align-items-center">

    <p class="pagi_text mb-3 mb-md-0 me-md-3">Showing {{ $paginator->currentPage() }} of {{ $paginator->lastPage() }} Pages</p>
    @if ($paginator->hasPages())
            <div class="d-flex align-items-center">

                @if($paginator->currentPage() > 1)
                    {{-- Previous Page Link --}}
                    <span wire:click="previousPage" class=" mx-1"><p class="pagi_item">Prev</p></span>
                @endif

            <!-- Pagination Elements -->
                @foreach ($elements as $element)
                <!-- Array Of Links -->
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                        <!--  Use three dots when current page is greater than 3.  -->
                            @if ($paginator->currentPage() > 3 && $page === 2)
                                <span class=" mx-1"><p class="pagi_item_dots"> . . . </p></span>
                            @endif

                        <!--  Show active page two pages before and after it.  -->
                            @if ($page == $paginator->currentPage())
                                <span wire:click="previousPage" class=" mx-1"><p class="pagi_item_active">{{$page}}</p></span>
                            @elseif ($page === $paginator->currentPage() + 1 || $page === $paginator->currentPage() + 2 || $page === $paginator->currentPage() - 1 || $page === $paginator->currentPage() - 2)
                                <span wire:click="gotoPage({{$page}})" class=" mx-1"><p class="pagi_item">{{$page}}</p></span>
                            @endif

                        <!--  Use three dots when current page is away from end.  -->
                            @if ($paginator->currentPage() < $paginator->lastPage() - 2  && $page === $paginator->lastPage() - 1)
                                <span class=" mx-1"><p class="pagi_item_dots"> . . . </p></span>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                        <span wire:click="nextPage" class=" mx-1"><p class="pagi_item">Next</p></span>
                @endif
            </div>
        @endif

</div>
