<div xmlns:wire="http://www.w3.org/1999/xhtml">
    @if($editcustomer)
        <!--Edit Supplier modal -->
            <div class="modal d-block modal_con" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <p class="f_head">Edit Customer</p>
                            <button wire:click="close_editcustomer_modal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form wire:submit.prevent="updatecustomer({{$customer_id}})">
                            @if (session()->has('message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    <strong class="table_msg">Successfully! {{ session('message') }} </strong>
                                </div>
                            @endif
                            <div class="modal-body">
                                <div class="d-md-flex ">
                                    <div class="col mb-3 me-md-3">
                                        <input wire:model.lazy="name" type="text" class="f_box" data-role="input" placeholder="Name" required>
                                        @error('name') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col mb-3">
                                        <input wire:model.lazy="phone" type="text" class="f_box" data-role="input" placeholder="Phone" required>
                                        @error('phone') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="d-md-flex ">
                                    <div class="col mb-3">
                                        <input wire:model.lazy="email" type="tel" class="f_box" data-role="input" placeholder="Email Address" required>
                                        @error('email') <span class="error">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col ">
                                    <textarea wire:model.lazy="address" class="f_area" placeholder="Store Address" required></textarea>
                                    @error('address') <span class="error">{{ $message }}</span> @enderror
                                    <div class="form-check form-switch">
                                        <input id="check" wire:model="status" type="checkbox" class="form-check-input" checked>
                                        <label for="check" class="form-check-label f_label">Active</label>
                                    </div>
                                </div>
                            </div>
                            <div class="f_footer">
                                <button wire:loading.remove wire:target="updatecustomer" type="submit" class="f_btn">Update</button>
                                <button wire:loading wire:target="updatecustomer" type="button" class="f_btn" disabled>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    Processing...
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    @endif
</div>
