<div class="d-flex mx-0" xmlns:wire="http://www.w3.org/1999/xhtml">

@if($order)
    <!-- Order modal -->
    <div class="modal d-block modal_con" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="f_head">Order Receipts</p>
                    <button wire:click="close_order" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form wire:submit.prevent="createcategory">
                    @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong class="table_msg">Successfully! {{ session('message') }} </strong>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="col mb-4">
                            <select wire:model="selectedcustomer" class="f_sel" >
                                <option value="{{ null }}" selected>Select a customer</option>
                                @foreach($customers as $customer)
                                    <option value="{{ $customer->name }}" selected>{{ $customer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="">
                            <div class="d-flex justify-content-between align-item-center mb-3">
                                <p class="order_price2">Customer</p>
                                <p class="order_price">{{ $selectedcustomer }}</p>
                            </div>
                            <div class="d-flex flex-justify-end mb-3">
                                <p class="order_price2">12-Aug-2021</p>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="order_text">Tracking Number</p>
                                <p class="order_price2">1292439hr23</p>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="order_text">Product's name</p>
                                <p class="order_price">$300</p>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="order_text">Product's name</p>
                                <p class="order_price">$300</p>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="order_text">Product's name</p>
                                <p class="order_price">$300</p>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="order_text">Product's name</p>
                                <p class="order_price">$300</p>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="order_text">Product's name</p>
                                <p class="order_price">$300</p>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="order_text">Product's name</p>
                                <p class="order_price">$300</p>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-between mb-3">
                                <p class="order_text1">Sub-Total</p>
                                <p class="order_price1">$300000</p>
                            </div>
                            <div class="d-flex justify-content-between ">
                                <p class="order_text1">Total</p>
                                <p class="order_price1">$3340000</p>
                            </div>
                        </div>
                    </div>
                    <div class="f_footer">
                        <button  wire:click="" type="submit" class="f_btn">Process</button>
                        <button  wire:click="" type="button" class="f_btn"> <i class="mif-print"></i> Print</button>
                        <button  wire:click="close_order" type="button" class="f_btn2 js-dialog-close">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif

    <div id="slide-out" class="nav_con d-lg-flex flex-column px-0">
        <div class="">
            <div class="col nav_logo_con d-flex justify-content-center px-3">
                <a href="{{ route('dashboard') }}" class="text-decoration-none flex-self-center "> <h4 class="nav_logo">Point Of Sales</h4> </a>
                <span id="close_menu" class="nav_bar d-lg-none flex-self-center ms-auto"> <i class="mif-arrow-left"></i> </span>
            </div>
        </div>
        <div class="pos_cont_con">
            @foreach($items as $key => $item)
                <div class="col pos_cont mb-1">
                    <p class="pos_text">{{ $item['name'] }}</p>
                    <div class="col pos_details d-flex justify-content-between align-items-center">
                        <p class="pos_head">₦ {{ $item['price'] }}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="ar_con me-3">
                                <span wire:click="increase" class="mif-plus ar_btn"></span>
                                <span class="ar_num">{{ $qty }}</span>
                                <span wire:click="descrease" class="mif-minus ar_btn"></span>
                            </div>
                            <span wire:click="remove({{ $key }}, {{$item['price']}} )" class="mif-bin pos_icon"></span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="pos_check">
            <div class="d-flex justify-content-between align-self-center mb-2">
                <p class="pos_head">Total Items</p>
                <p class="pos_head">{{ count($items) }}</p>
            </div>
            <div class="d-flex justify-content-between align-self-center mb-2">
                <p class="pos_head">SubTotal</p>
                <p class="pos_head">₦ {{ $subtotal . '.00' }}</p>
            </div>
            <div class="d-flex justify-content-between align-self-center mb-2">
                <p class="pos_head">Total</p>
                <p class="pos_head">₦ {{ $total . '.00' }}</p>
            </div>
            <button wire:click="checkout" type="button" class="pos_check_btn" >Checkout</button>
        </div>
    </div>
    <div class="col nav_cont px-0">
        <div class="col nav_cont_header_con d-flex justify-content-between align-items-center sticky-top px-3">
            <div class="d-flex ">
                <span id="mob_menu" class="nav_cont_header_icon d-lg-none flex-self-center me-3"> <i class="mif-menu"></i> </span>
                <h4 class="nav_cont_header flex-self-center">Our Products</h4>
            </div>
            <div class="d-flex ">
                <a href="{{ route('dashboard') }}" class="text-decoration-none ms-2 ms-md-4"> <span class="nav_cont_header_icon " title="Dsahbaord"> <i class="mif-dashboard"></i> </span></a>
                <a href="{{ route('messages') }}" class="text-decoration-none ms-2 ms-md-4"> <span class="nav_cont_header_icon position-relative" title="Messages"> <i class="mif-envelop"></i> <span class="position-absolute top-0 start-100 translate-middle badge rounded-circle bg-warning p-1"> </span> </span>  </a>
                <a href="{{ route('mails') }}" class="text-decoration-none ms-2 ms-md-4"> <span class="nav_cont_header_icon position-relative" title="Notification"> <i class="mif-bell"></i> <span class="position-absolute top-0 start-100 translate-middle badge rounded-circle bg-warning p-1"> </span> </span> </a>

            </div>
        </div>
        <div class="">
            <div class="col col-md-10 pos_sea_con mx-auto d-flex mb-4">
                <input wire:model.debounce.500ms="search" type="search" class="pos_sea" placeholder="Search For Products...">
                <button type="button" class="pos_sea_btn d-flex "><i class="mif-search"></i></button>
            </div>
            <div class="d-flex justify-content-center align-self-center mb-4">
                <div wire:loading wire:target="search" class="">
                    <div class="spinner-grow text-primary" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                    <div class="spinner-grow text-danger" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                    <div class="spinner-grow text-warning" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
                @if($products->count() < 1)
                    <p wire:loading.remove wire:target="search" class="t_res">No Record Found...</p>
                @endif
            </div>
            <div class="row mx-0">
                @foreach($products as $product)
                    <div class="col-12 col-sm-6 col-md-4 col-xl-3 mb-4">
                        <div class="col pos_sel_cont d-flex px-0">
                            <div class="col-4 pos_sel_img_con px-0">
                                <img src="{{ asset('storage/products/'.$product->image) }}" class="pos_sel_img">
                            </div>
                            <div class="col pos_sel_details">
                                <p class="pos_sel_text">{{ $product->name }}</p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <p class="pos_sel_head">₦{{ $product->price }}</p>
                                    <button wire:click="add({{ $product->id }})" type="button" class="pos_sel_btn">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
