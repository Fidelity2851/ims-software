<div class="col nav_cont_con px-3">
    <div class="row justify-content-center mx-0 mb-3">
        <div class="col-12 col-sm-6 col-lg px-0 ps-sm-0 pe-sm-2 mb-4 px-sm-3">
            <div class="col nav_dash_cont d-flex flex-column px-3">
                <p class="nav_cont_header">Total Sales</p>

                <div class="nav_cont_details mt-auto">
                    <p class="nav_cont_num">
                        {{ $sales }}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-lg px-0 pe-sm-0 ps-sm-2 px-lg-2 mb-4 px-sm-3">
            <div class="col nav_dash_cont d-flex flex-column px-3">
                <p class="nav_cont_header">Total Profit</p>

                <div class="nav_cont_details mt-auto">
                    <p class="nav_cont_num">
                        {{ $profits }}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-lg px-0 ps-sm-0 pe-sm-2 px-lg-2 mb-4 px-sm-3">
            <div class="col nav_dash_cont d-flex flex-column px-3">
                <p class="nav_cont_header">Total Suppliers</p>

                <div class="nav_cont_details mt-auto">
                    <p class="nav_cont_num">{{ $suppliers }}</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-lg px-0 pe-sm-0 ps-sm-2 mb-4 px-sm-3">
            <div class="col nav_dash_cont d-flex flex-column px-3">
                <p class="nav_cont_header">Month's Salary </p>

                <div class="nav_cont_details d-flex justify-content-between align-items-center mt-auto">
                    <p class="nav_cont_num ">
                        {{ $salary }}
                    </p>
                    <button type="button" class="nav_cont_status">Paid <span class="mif-verified"></span> </button>
                </div>
            </div>
        </div>
    </div>
    <div class="d-lg-flex justify-content-between">
        <div class="col col-lg-8 nav_dash_graph p-4 mb-0-md mb-4 mb-lg-0">
            <div class="d-flex mb-4">
                <p class="nav_cont_header">Monthly Revenue Growth</p>
            </div>
            <canvas class="" id="myChart" style="width: 100%; height: 300px"></canvas>
        </div>
        <div class="col nav_dash_graph ms-lg-4 p-4">
            <div class="d-flex ">
                <p class="nav_cont_header">Revenue</p>
            </div>
        </div>
    </div>
</div>

