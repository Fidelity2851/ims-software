<div class="col nav_cont_con px-3" xmlns:wire="http://www.w3.org/1999/xhtml">


    <!--Sub-Category modal -->
{{--<div class="modal d-block modal_con" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="f_head">Create Categories</p>
                    <button wire:click="" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form wire:submit.prevent="">
                    @if (session()->has('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong class="table_msg">Successfully! {{ session('message') }} </strong>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="d-md-flex ">
                            <div class="col mb-3 mr-md-3">
                                <input wire:model.lazy="name" type="text" class="f_box" data-role="input" placeholder="Name" required>
                                @error('name') <span class="error">{{ $message }}</span> @enderror
                            </div>
                            <div class="col mb-3">
                                <select wire:model="category" class="f_sel" multiple >
                                    <option disabled>Categories</option>
                                    <option >Categories</option>
                                    <option >Categories</option>
                                </select>
                                @error('cate') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col ">
                            <textarea wire:model.lazy="description" class="f_area" placeholder="Description" required></textarea>
                            @error('description') <span class="error">{{ $message }}</span> @enderror
                            <div class="form-check form-switch">
                                <input id="check" wire:model="status" type="checkbox" class="form-check-input" checked>
                                <label for="check" class="form-check-label f_label">Active</label>
                            </div>
                        </div>
                    </div>
                    <div class="f_footer">
                        <button wire:loading.remove wire:target="" type="submit" class="f_btn">Create</button>
                        <button wire:loading wire:target="" type="button" class="f_btn" disabled>
                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            Processing...
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>--}}
<!--Delete Category modal -->
    {{--<div class="modal d-block modal_con" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <p class="f_head">Confirm Deleting Category</p>
                    <button wire:click="" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p class="f_label">Note: Deleting this category will also delete all its relationships!</p>
                </div>
                <div class="f_footer mt-4">
                    <button wire:loading.attr="disabled" wire:click="" type="button" class="f_btn">Confirm</button>
                    <button wire:click="" type="button" class="f_btn2 js-dialog-close">Cancel</button>
                </div>
            </div>
        </div>
    </div>--}}


    <div class="col d-flex flex-column-reverse flex-md-row justify-content-md-between align-items-center mb-4">
        <div class="">
            <button wire:click="show_category_modal" type="button" class="c_btn">Create</button>
        </div>
        <div class="bread_link_con ">
            <a href="dashboard.blade.php" class="text-decoration-none"><span class="bread_link">Dashboard / </span> </a>
            <span class="bread_link">Category</span>
        </div>
    </div>
    <div class="">
        @if (session()->has('del_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong class="table_msg">Successfully! {{ session('del_message') }} </strong>
            </div>
        @endif
        <div class="col navbar navbar-expand-md d-flex justify-content-between align-items-center mb-3 ">
            <form class="col col-md-4 px-0-md me-3">
                <input wire:model.debounce.500ms="search" type="search" class="table_box" placeholder="Search for categories...">
            </form>
            <button class="table_btn navbar-toggler border p-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="mif-filter"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end mt-3 mt-md-0" id="navbarSupportedContent">
                <div class="d-flex overflow-auto px-md-0 ">
                    <select class="table_sel" >
                        <option disabled selected>Type</option>
                        <option>All</option>
                        <option>Active</option>
                        <option >Pending</option>
                    </select>
                    <select class="table_sel ms-2 " data-role="select">
                        <option disabled selected>Arrange</option>
                        <option>Assending</option>
                        <option >Desending</option>
                    </select>
                    <select class="table_sel ms-2" data-role="select">
                        <option disabled selected>Number per page</option>
                        <option>15</option>
                        <option >20</option>
                        <option >25</option>
                        <option >30</option>
                        <option >35</option>
                        <option >40</option>
                        <option >45</option>
                        <option >50</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-center align-self-center mb-2">
            <div class="" wire:loading wire:target="search">
                <div class="spinner-grow text-primary" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-danger" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            </div>
            @if(false)
                <p class="t_res " wire:loading.remove wire:target="search" >No Record Found...</p>
            @endif
        </div>

        <!--Table-->
        <div class="table-responsive-md">
            <table class="table table-striped mb-4">
                <thead class="t_head_con">
                <tr>
                    <th class="t_head">#</th>
                    <th class="t_head">
                        <input id="main_check" type="checkbox" class="form-check-input">
                    </th>
                    <th class="t_head">Category</th>
                    <th class="t_head">Description</th>
                    <th class="t_head">Sub-Category</th>
                    <th class="t_head">Status</th>
                    <th class="t_head">Manage</th>
                </tr>
                </thead>
                <tbody >
                {{--<tr>
                    <td class="t_data">{{ $loop->iteration }}</td>
                    <td>
                        <input type="checkbox" wire:model="check" value="{{ $category->id }}"  class="form-check-input">
                    </td>
                    <td class="t_data">{{ $category->name }}</td>
                    <td class="t_data">{{ $category->description }}</td>
                    <td class="t_data">{{ $category->subcategory()->count() }}</td>
                    <td>
                        @if($category->status == 1)
                            <span class="t_status1">Active</span>
                        @else
                            <span class="t_status2">pending</span>
                        @endif
                    </td>
                    <td class="d-flex">
                        <span wire:click="edit_category" title="Edit" class="mif-pencil t_icon "></span>
                        <span wire:click="show_delete_modal({{ $category->id }})" title="Delete" class="mif-bin t_icon2"></span>
                    </td>
                </tr>--}}
                </tbody>
            </table>
        </div>

        <div class="d-md-flex justify-content-between align-items-end">
            <div class="d-flex mb-3 mb-md-0">
                <button wire:click="multi_delete_category" type="button" class="t_btn_del flex-self-center me-2">Delete</button>
                <button type="button" class="t_btn_pen flex-self-center me-2">Pending</button>
                <button type="button" class="t_btn_app flex-self-center">Approve</button>
            </div>
            <div class="d-xl-flex align-items-center">
                <p class="pagi_text mb-3 mb-md-0 me-md-3">Showing 1 to 15 of 200</p>
                <div class="d-flex align-items-center">
                    <a href="#" class="text-decoration-none mx-1"><p class="pagi_item">Prev</p></a>
                    <p class="pagi_item_active mx-1">1</p>
                    <a href="#" class="text-decoration-none mx-1"><p class="pagi_item">2</p></a>
                    <a href="#" class="text-decoration-none mx-1"><p class="pagi_item">3</p></a>
                    <a href="#" class="text-decoration-none mx-1"><p class="pagi_item">Next</p></a>
                </div>
            </div>
        </div>
    </div>
</div>
