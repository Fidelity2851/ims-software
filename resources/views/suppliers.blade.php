@extends('layouts.layout')

@section('title', 'Suppliers')

@section('content')
    <livewire:suppliers />
@endsection
