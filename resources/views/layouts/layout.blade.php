<!DOCTYPE html>
<html lang="en" xmlns:livewire="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>POS - @yield('title')</title>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;800&family=Overpass:wght@200;300;400;600;700;800&display=swap" rel="stylesheet">

    <!--font awesome-->
    {{-- <script src="https://kit.fontawesome.com/a076d05399.js"></script> --}}

    <!-- Styleshheet -->
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

    <!--icons-->
    <link href="{{ asset('css/metro-icons.css') }}" rel="stylesheet">


    {{--livewire--}}
    <livewire:styles />
</head>
<body>
<!--housing DIV-->
<div class="d-flex housing ">

    <div id="slide-out" class="nav_con d-lg-flex flex-column sticky-lg-top px-0">
        <div class="">
            <div class="col nav_logo_con d-flex justify-content-center align-items-center px-3">
                <a href="{{ route('dashboard') }}" class="text-decoration-none "> <h4 class="nav_logo">POS System</h4> </a>
                <span id="close_menu" class="nav_bar d-lg-none ms-auto"> <i class="mif-arrow-left"></i> </span>
            </div>
            <div class="nav_user_con d-flex overflow-hidden">
                @if(!empty(auth()->user()->image))
                    <img src="{{ asset('storage/workers/' . auth()->user()->image) }}" class="nav_user_img align-self-center me-3">
                @else
                    <img src="{{ asset('images/custom.jpg') }}" class="nav_user_img align-self-center me-3">
                @endif
                <div class="align-self-center">
                    <p class="nav_user_name">{{ auth()->user()->name  }}</p>
                    <div class="d-flex align-items-center">
                        <p class="nav_user_role ">{{ auth()->user()->roles->name }}</p>
                        @if(Auth::check())
                            <form action="{{ route('logout') }}" method="post" class="d-sm-none ms-3 ">
                                @csrf
                                <button type="submit" class="logout_btn">Logout</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!--nav links-->
        <div class="overflow-auto" >
            <div class="nav_link_con ">
                <a href="{{ route('dashboard') }}" class="text-decoration-none text-decoration-none">
                    <p class="nav_link waves-light "> <span class="align-self-center"> <i class="mif-dashboard nav_link_icon me-3"></i> Dashboard</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('pos') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-shopping-basket nav_link_icon me-3"></i> POS</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('category') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-flow-tree nav_link_icon me-3"></i> Category</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('products') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-widgets nav_link_icon me-3"></i> Products</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('purchases') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-credit-card nav_link_icon me-3"></i> Purchases</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('customers') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-users nav_link_icon me-3"></i> Customers</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('suppliers') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-users nav_link_icon me-3"></i> Suppliers</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('workers') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-user-check nav_link_icon me-3"></i> Workers</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('mails') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-envelop nav_link_icon me-3"></i> Mails</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('messages') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-bubbles nav_link_icon me-3"></i> Messages</span> </p>
                </a>
            </div>
            <div class="nav_link_con ">
                <a href="{{ route('accounts') }}" class="text-decoration-none">
                    <p class="nav_link"> <span class="align-self-center"> <i class="mif-dollars nav_link_icon me-3"></i> Accounts</span> </p>
                </a>
            </div>
            <div id="set" class="nav_link_con ">
                <p class="nav_link d-flex justify-content-between"> <span class="align-self-center"> <i class="mif-cogs nav_link_icon me-3"></i>Setting</span> <i class="mif-arrow-drop-down nav_link_icon align-self-center ml-auto"></i></p>
                <!-- Collapsible element -->
                <div id="set_drop" class="nav_drop_con" >
                    <a href="{{ route('gen_setting') }}" class="text-decoration-none">
                        <p class="nav_drop_link">General</p>
                    </a>
                    <a href="#" class="text-decoration-none">
                        <p class="nav_drop_link">Permissions</p>
                    </a>
                    <a href="{{ route('roles') }}" class="text-decoration-none">
                        <p class="nav_drop_link">Manage Roles</p>
                    </a>
                    <a href="{{ route('subcategory') }}" class="text-decoration-none">
                        <p class="nav_drop_link">Manage Sub-category</p>
                    </a>
                </div>
            </div>
        </div>

        <div class="nav_foot mt-auto">
            <p class="nav_foot_text">
                &copy; 2020 POS <br>
                Created by Tanatechlabs LTD
            </p>
        </div>

    </div>
    <div class="col nav_cont px-0">
        <div class="col nav_cont_header_con d-flex justify-content-between align-items-center sticky-top px-3">
            <div class="d-flex align-items-center">
                <span id="mob_menu" class="nav_cont_header_icon d-lg-none me-3"> <i class="mif-menu"></i> </span>
                <h4 class="nav_cont_header">@yield('title')</h4>
            </div>
            <div class="d-flex ">
                <a href="{{ route('messages') }}" class="text-decoration-none ms-3 "> <span class="nav_cont_header_icon position-relative"> <i class="mif-envelop"></i> <span class="position-absolute top-0 start-100 translate-middle badge rounded-circle bg-warning p-1"> </span> </span> </a>
                <a href="{{ route('mails') }}" class="text-decoration-none ms-3 "> <span class="nav_cont_header_icon position-relative"> <i class="mif-bell"></i> <span class="position-absolute top-0 start-100 translate-middle badge rounded-circle bg-warning p-1"> </span> </span> </a>
                @if(Auth::check())
                    <form action="{{ route('logout') }}" method="post" class="d-none d-sm-block ms-3 ">
                        @csrf
                        <button type="submit" class="logout_btn">Logout</button>
                    </form>
                @endif
            </div>
        </div>

        <div class="col px-0">
            @yield('content')
        </div>

    </div>
</div>

{{--livewire--}}
<livewire:scripts />

<!-- Javascript Files -->
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/popper.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/index.js') }}"></script>

<!-- chart.js -->
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script>
    // chart.js
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [{
            label: 'Monthly Revenue Growth',
            backgroundColor: 'yellow',
            borderColor: 'white',
            data: [5, 10, 5, 2, 20, 30, 45, 2, 20, 30, 45]
        }]
    },

    // Configuration options go here
    options: {}
    });

</script>    


</body>
</html>
