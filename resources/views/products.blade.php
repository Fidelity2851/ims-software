@extends('layouts.layout')

@section('title', 'Products')

@section('content')
    <livewire:products />
@endsection
